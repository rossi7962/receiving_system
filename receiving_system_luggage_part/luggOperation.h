#ifndef RECEIVING_SYSTEM_LUGGAGE_PART_LUGGOPERATION_H
#define RECEIVING_SYSTEM_LUGGAGE_PART_LUGGOPERATION_H

#define STATUS_LOCKED 1
#define STATUS_UNLOCKED 2

#define BOX_NUMBER 5

struct BoxData {
    int status;
};

struct BoxData box[BOX_NUMBER];

void initializeLuggagePart ();
int setStatusAtIndex (int index, int status);

#endif //RECEIVING_SYSTEM_LUGGAGE_PART_LUGGOPERATION_H
