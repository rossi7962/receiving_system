#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#define MAXLINE 4096 /*max text line length*/
#define SERV_PORT 3000 /*port*/
#define LISTENQ 8 /*maximum number of client connections */

#define UNLOCK_BOX "UNLOCK_BOX"
#define OPEN_BOX "OPEN_BOX"
#define CLOSE_BOX "CLOSE_BOX"
#define LOCK_BOX "LOCK_BOX"

#define SUCCESS "SUCCESS"
#define FAIL "FAIL"

#include "luggOperation.h"

int connfd;

int main(int argc, char **argv) {
    //init luggage
    initializeLuggagePart();

    //socket variable
    int listenfd, n;
    pid_t childpid;
    socklen_t clilen;
    char buf[MAXLINE];
    memset(buf, 0, MAXLINE);
    struct sockaddr_in cliaddr, servaddr;

    //creation of the socket
    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    //preparation of the socket address
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(SERV_PORT);

    bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

    listen(listenfd, LISTENQ);

    printf("%s\n", "Server running...waiting for connections.");

    for (;;) {

        clilen = sizeof(cliaddr);
        connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen);
        printf("%s\n", "Received request...");

        while ((n = recv(connfd, buf, MAXLINE, 0)) > 0) {
            //handle message here
            printf("%s", "String received from and resent to the client: ");
            puts(buf);

            //get cmd id
            char *tok = NULL;
            tok = strtok(buf, " ");
            char cmd[100];
            strcpy(cmd, tok);

            //handle seperate cmd id

            //CMD: UNLOCK_BOX
            if (strcmp(UNLOCK_BOX, cmd) == 0) {
                tok = strtok(NULL, " ");
                char boxIndexStr[100];
                strcpy(boxIndexStr, tok);
                int boxIndex = atoi(boxIndexStr);

                if (box[boxIndex].status == STATUS_UNLOCKED) send(connfd, FAIL, 100, 0);
                else if (setStatusAtIndex(boxIndex, STATUS_UNLOCKED)) {
                    send(connfd, SUCCESS, 100, 0);
                } else send(connfd, FAIL, 100, 0);
            }

            //CMD: LOCK_BOX
            else if (strcmp(LOCK_BOX, cmd) == 0) {
                tok = strtok(NULL, " ");
                char boxIndexStr[100];
                strcpy(boxIndexStr, tok);
                int boxIndex = atoi(boxIndexStr);

                if (box[boxIndex].status == STATUS_LOCKED) send(connfd, FAIL, 100, 0);
                else if (setStatusAtIndex(boxIndex, STATUS_LOCKED)) {
                    send(connfd, SUCCESS, 100, 0);
                } else send(connfd, FAIL, 100, 0);
            }

            //DEFAULT: UNDEFINED PACKAGE
            else {
                send(connfd, "ERROR UNDEFINED PACKAGE", 100, 0);
            }
            memset(buf, 0, MAXLINE);
        }

        if (n < 0) {
            perror("Read error");
            exit(1);
        }
        close(connfd);

    }

    //p listening socket
    close(listenfd);
}