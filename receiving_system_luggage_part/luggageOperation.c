#include "luggOperation.h"

void initializeLuggagePart () {
    for (int i = 0; i < BOX_NUMBER; i += 1) {
        box[i].status = STATUS_LOCKED;
    }
}

int setStatusAtIndex (int index, int status) {
    if (index >= BOX_NUMBER) return 0;
    else {
        box[index].status = status;
        return 1;
    }
}