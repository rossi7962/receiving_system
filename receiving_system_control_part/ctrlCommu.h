//
// Created by MacBook Pro on 6/30/20.
//

#ifndef RECEIVING_SYSTEM_CONTROL_PART_CTRLCOMMU_H
#define RECEIVING_SYSTEM_CONTROL_PART_CTRLCOMMU_H

#define UNLOCK_BOX "UNLOCK_BOX"
#define LOCK_BOX "LOCK_BOX"

#define SUCCESS "SUCCESS"
#define FAIL "FAIL"

int ctrlCommu_TCPConnect ();
void ctrlCommu_getResponseFromLuggagePartWithMessage (char* msg, char* receive);
void ctrlCommu_sendMessageUnlockBox(int boxNumber, char* receive);
void ctrlCommu_sendMessageLockBox(int boxNumber, char* receive);

#endif //RECEIVING_SYSTEM_CONTROL_PART_CTRLCOMMU_H
