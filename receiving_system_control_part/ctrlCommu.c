#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h> 
#include <string.h>
#include "ctrlCommu.h"

#define MAXLINE 4096 /*max text line length*/
#define SERV_PORT 3000 /*port*/

int sockfd;

int ctrlCommu_TCPConnect() {
    struct sockaddr_in servaddr;
    char sendline[MAXLINE], recvline[MAXLINE];

    //Create a socket for the client
    //If sockfd<0 there was an error in the creation of the socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("ctrlCommu | Problem in creating the socket");
        exit(2);
    }

    //Creation of the socket
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(SERV_PORT); //convert to big-endian order

    //Connection of the client to the socket
    if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
        perror("ctrlCommu | Problem in connecting to the server");
        exit(3);
    }
}

void ctrlCommu_getResponseFromLuggagePartWithMessage (char* msg, char* receive) {
    send(sockfd, msg, strlen(msg), 0);
    if (recv(sockfd, receive, MAXLINE, 0) == 0) {
        printf("ctrlCommu | sendMgr | The server terminated prematurely");
    }
}

void ctrlCommu_sendMessageUnlockBox(int boxNumber, char* receive) {
    char msg[100];
    memset(msg, 0, 100);
    strcpy(msg, UNLOCK_BOX);

    char boxNumberBuffer[50];
    snprintf(boxNumberBuffer, sizeof boxNumberBuffer, "%d", boxNumber);
    strcat(msg," ");
    strcat(msg, boxNumberBuffer);

    ctrlCommu_getResponseFromLuggagePartWithMessage(msg, receive);
}

void ctrlCommu_sendMessageLockBox(int boxNumber, char* receive) {
    char msg[100];
    memset(msg, 0, 100);
    strcpy(msg, LOCK_BOX);

    char boxNumberBuffer[50];
    snprintf(boxNumberBuffer, sizeof boxNumberBuffer, "%d", boxNumber);
    strcat(msg," ");
    strcat(msg, boxNumberBuffer);

    ctrlCommu_getResponseFromLuggagePartWithMessage(msg, receive);
}
