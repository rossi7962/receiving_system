#include "ctrlData.h"
#include "stdio.h"
#include <stdlib.h>

void ctrlData_initData()
{
    for (int i = 0; i < MAX_GOOD_CONTAIN; i += 1)
        ctrlData_resetGoodData(&arrivalGoodDataArray[i]);
}

struct ArrivalGoodData *ctrlData_getGoodByBoxNumber(int boxNumber)
{
    struct ArrivalGoodData *goodsInBox = malloc(sizeof(struct ArrivalGoodData) * MAX_GOOD_CONTAIN);
    int j = 0;

    for (int i = 0; i < MAX_GOOD_CONTAIN; i += 1)
    {
        ctrlData_resetGoodData(&goodsInBox[i]);
        if (boxNumber == arrivalGoodDataArray[i].boxNumber)
        {
            goodsInBox[j++] = arrivalGoodDataArray[i];
        }
    }
    return goodsInBox;
}

struct ArrivalGoodData *ctrlData_getGoodByRoomNumber(int roomNumber)
{
    struct ArrivalGoodData *goodsOfRoom = malloc(sizeof(struct ArrivalGoodData) * MAX_GOOD_CONTAIN);
    int j = 0;

    for (int i = 0; i < MAX_GOOD_CONTAIN; i += 1)
    {
        ctrlData_resetGoodData(&goodsOfRoom[i]);
        if (roomNumber == arrivalGoodDataArray[i].roomNumber)
        {
            goodsOfRoom[j++] = arrivalGoodDataArray[i];
        }
    }
    return goodsOfRoom;
}

int ctrlData_addGoodToArray(int boxNumber, int roomNumber)
{
    for (int i = 0; i < MAX_GOOD_CONTAIN; i += 1)
    {
        if (arrivalGoodDataArray[i].roomNumber == -1 &&
            arrivalGoodDataArray[i].boxNumber == -1 &&
            arrivalGoodDataArray[i].id == -1)
        {
            arrivalGoodDataArray[i].id = i;
            arrivalGoodDataArray[i].roomNumber = roomNumber;
            arrivalGoodDataArray[i].boxNumber = boxNumber;
            return 1;
        }
    }

    return 0;
}

struct ArrivalGoodData *goodDataById (int index) {
    return &arrivalGoodDataArray[index];
}

void ctrlData_resetGoodData(struct ArrivalGoodData *goodData)
{
    goodData->id = -1;
    goodData->boxNumber = -1;
    goodData->roomNumber = -1;
}

int ctrlData_printGoodDataArray()
{
    int count = 0;
    for (int i = 0; i < MAX_GOOD_CONTAIN; i += 1)
    {
        if (arrivalGoodDataArray[i].roomNumber != -1)
        {
            count++;
            printf("%d. RoomNumber: %d , boxNumber: %d\n",
                   i,
                   arrivalGoodDataArray[i].roomNumber,
                   arrivalGoodDataArray[i].boxNumber);
        }
    }
    return count;
}

int ctrlData_printGoodsOfArray(struct ArrivalGoodData *goods)
{
    int count = 0;
    for (int i = 0; i < MAX_GOOD_CONTAIN; i += 1)
    {
        if (goods[i].roomNumber != -1)
        {
            count++;
            printf("%d. Identifier: %d RoomNumber: %d , boxNumber: %d\n",
                   i,
                   goods[i].id,
                   goods[i].roomNumber,
                   goods[i].boxNumber);
        }
    }
    return count;
}


