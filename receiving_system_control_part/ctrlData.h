#ifndef RECEIVING_SYSTEM_CTRLDATA_H
#define RECEIVING_SYSTEM_CTRLDATA_H

#define MAX_GOOD_CONTAIN 10
struct ArrivalGoodData {
    int id;
    int boxNumber;
    int roomNumber;
};

struct ArrivalGoodData arrivalGoodDataArray[MAX_GOOD_CONTAIN];

void ctrlData_initData ();
struct ArrivalGoodData* ctrlData_getGoodByBoxNumber (int boxNumber);
struct ArrivalGoodData* ctrlData_getGoodByRoomNumber (int roomNumber);
struct ArrivalGoodData* goodDataById (int index);
int ctrlData_addGoodToArray (int boxNumber, int roomNumber);
void ctrlData_resetGoodData (struct ArrivalGoodData* goodData);
int ctrlData_printGoodDataArray ();
int ctrlData_printGoodsOfArray(struct ArrivalGoodData *goods);

#endif //RECEIVING_SYSTEM_CTRLDATA_H
