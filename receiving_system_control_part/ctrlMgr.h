#ifndef RECEIVING_SYSTEM_CTRLMGR_H
#define RECEIVING_SYSTEM_CTRLMGR_H

void ctrlMgr_appStart ();
void ctrlMgr_flowArrivalOfLuggage ();
void ctrlMgr_flowTakingOutOfLuggage ();
void ctrlMgr_waitingForCommandFromKeyboard ();

#endif //RECEIVING_SYSTEM_CTRLMGR_H
