#include "ctrlMgr.h"
#include "ctrlCommu.h"
#include "ctrlData.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

void ctrlMgr_appStart()
{
    printf("ctrlMgr | ctrlMgr_appStart\n");
    ctrlCommu_TCPConnect();
    ctrlData_initData();
    ctrlMgr_waitingForCommandFromKeyboard();
}

void ctrlMgr_waitingForCommandFromKeyboard()
{
    int option = -1;
    while (option != 0)
    {
        printf("\n\nReceiving System\n");
        printf("1. Good Arrival\n");
        printf("2. Receive Good\n");
        printf("0. Quit\n");
        scanf("%d", &option);
        printf("selected: %d\n", option);

        switch (option)
        {
        case 1:
            ctrlMgr_flowArrivalOfLuggage();
            break;
        case 2:
            ctrlMgr_flowTakingOutOfLuggage();
            break;
        case 0:
            printf("See you again!\n");
            break;
        default:
            printf("Wrong input, shutting down!\n");
            option = 0;
        }
    }
};

void ctrlMgr_flowArrivalOfLuggage()
{
    printf("ctrlMgr | ctrlMgr_flowArrivalOfLuggage\n");

    //INPUT BOX NUMBER
    int boxNumber;
    printf("Please input box number: ");
    scanf("%d", &boxNumber);

    //SEND REQUEST TO LUGGAGE PART
    char response[100];
    memset(response, 0, 100);
    ctrlCommu_sendMessageUnlockBox(boxNumber, response);
    printf("response: %s\n", response);

    if (strcmp(response, FAIL) == 0)
    {
        printf("UNLOCK BOX %d FAILED\n", boxNumber);
        return;
    }

    //INPUT ROOM NUMBER
    int roomNumber;
    printf("Please input room number: ");
    scanf("%d", &roomNumber);

    //SAVE DATA
    int addSuccess = ctrlData_addGoodToArray(boxNumber, roomNumber);
    if (!addSuccess) {
        printf("Receiving System full, come back later!\n");
    } else {
        printf("Luggage Received, locking box\n");
    }

    //LOCKING BOX
    memset(response, 0, 100);
    ctrlCommu_sendMessageLockBox(boxNumber, response);
    printf("response: %s\n", response);
    if (strcmp(response, FAIL) == 0) {
        printf("UNLOCK BOX %d FAILED\n", boxNumber);
        return;
    }
}
void ctrlMgr_flowTakingOutOfLuggage()
{
    printf("ctrlMgr | ctrlMgr_flowTakingOutOfLuggage\n");

    // INPUT ROOM NUMBER
    int roomNumber;
    printf("Please input room number: ");
    scanf("%d", &roomNumber);

    // SHOW DATE AND COURIER OF ALL LUGGAGE
    struct ArrivalGoodData *goodsOfRoom = ctrlData_getGoodByRoomNumber(roomNumber);
    int count = ctrlData_printGoodsOfArray(goodsOfRoom);
    if (!count) {
        printf("Room %d has no goods available.\n", roomNumber);
        return;
    }

    // INPUT NUMBER OF ARRIVAL OF GOODS
    int index;
    printf("Please input index of arrival of goods: ");
    scanf("%d", &index);
    while(index > count - 1 || index < 0){
        printf("Selected index is out of range. Please input number of arrival of goods: ");
        scanf("%d", &index);
    }

    // TRANSMIT UNLOCKING MESSAGE TO BOX
    int selectedBoxNumber = goodsOfRoom[index].boxNumber;
    printf("Box number selected: %d\n", selectedBoxNumber);

    char response[100];
    memset(response, 0, 100);
    ctrlCommu_sendMessageUnlockBox(selectedBoxNumber, response);
    printf("response: %s\n", response);

    if (strcmp(response, FAIL) == 0)
    {
        printf("LOCK BOX %d FAILED\n", selectedBoxNumber);
        return;
    }

    // TAKE OUT LUGGAGE SO RESET DATA
    struct ArrivalGoodData* good = goodDataById(goodsOfRoom[index].id);
    ctrlData_resetGoodData(good);
}